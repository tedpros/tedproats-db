-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 28, 2020 at 12:27 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1
-- Sharmistha Email templates alter script to change email contant start

UPDATE `tedpros_email_template` SET `template` = '<html>\r\n\r\n<body>Hi REPORTINGNAME ,<br /> USERNAME has submitted the time sheets<br /><br />Actions: Approve, Reject<br />\r\n    <table style=\'border:1px solid black;\'>\r\n        <tr>\r\n            <th\r\n                style=\'padding-top: 12px;padding-bottom: 12px;text-align: center;background-color: BGCOLOR;color: white;\'>\r\n                Date(s)</th>\r\n            <th\r\n                style=\'padding-top: 12px;padding-bottom: 12px;text-align: center;background-color: BGCOLOR;color: white;\'>\r\n                Project</th>\r\n            <th\r\n                style=\'padding-top: 12px;padding-bottom: 12px;text-align: center;background-color: BGCOLOR;color: white;\'>\r\n                Activity</th>\r\n            <th\r\n                style=\'padding-top: 12px;padding-bottom: 12px;text-align: center;background-color: BGCOLOR;color: white;\'>\r\n                Comment</th>\r\n        </tr> TIMESHEETDATA\r\n    </table><br /><a href=\'baseUrl/\' target=\'_blank\'><button\r\n            style=\'padding:10px 5px; color:white;background-color:green;\'>Update Status</button></a><br />Thank\r\n    you.<br />This is an automated email.<br />\r\n</body>\r\n\r\n</html>' WHERE `tedpros_email_template`.`templateFor` = 'timesheet_submit';

-- Sharmistha Email templates alter script to change email contant End
