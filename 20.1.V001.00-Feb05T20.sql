-- version : 20.1.V001.00  5th Feb,2020

-- alter script to add new column for middle name - Sharmistha start

ALTER TABLE `tedpros_candidate`  ADD `middle_name` VARCHAR(32) NOT NULL  AFTER `first_name`;
