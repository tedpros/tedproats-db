-- version : 20.1.V001.00  2nd Feb,2020

-- alter email template for addcandidateprofile, timesheet Sharmistha start


UPDATE `tedpros_email_template` SET `template` = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <title>Tedpros</title>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }
        
        table,
        td,
        tr {
            vertical-align: top;
            border-collapse: collapse;
        }
        
        * {
            line-height: inherit;
        }
        
        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }
        
        .ie-browser table {
            table-layout: fixed;
        }
        
        [owa] .img-container div,
        [owa] .img-container button {
            display: block !important;
        }
        
        [owa] .fullwidth button {
            width: 100% !important;
        }
        
        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }
        
        .ie-browser .block-grid,
        .ie-browser .num12,
        [owa] .num12,
        [owa] .block-grid {
            width: 650px !important;
        }
        
        .ie-browser .mixed-two-up .num4,
        [owa] .mixed-two-up .num4 {
            width: 216px !important;
        }
        
        .ie-browser .mixed-two-up .num8,
        [owa] .mixed-two-up .num8 {
            width: 432px !important;
        }
        
        .ie-browser .block-grid.two-up .col,
        [owa] .block-grid.two-up .col {
            width: 324px !important;
        }
        
        .ie-browser .block-grid.three-up .col,
        [owa] .block-grid.three-up .col {
            width: 324px !important;
        }
        
        .ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
            width: 162px !important;
        }
        
        .ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
            width: 130px !important;
        }
        
        .ie-browser .block-grid.six-up .col,
        [owa] .block-grid.six-up .col {
            width: 108px !important;
        }
        
        .ie-browser .block-grid.seven-up .col,
        [owa] .block-grid.seven-up .col {
            width: 92px !important;
        }
        
        .ie-browser .block-grid.eight-up .col,
        [owa] .block-grid.eight-up .col {
            width: 81px !important;
        }
        
        .ie-browser .block-grid.nine-up .col,
        [owa] .block-grid.nine-up .col {
            width: 72px !important;
        }
        
        .ie-browser .block-grid.ten-up .col,
        [owa] .block-grid.ten-up .col {
            width: 60px !important;
        }
        
        .ie-browser .block-grid.eleven-up .col,
        [owa] .block-grid.eleven-up .col {
            width: 54px !important;
        }
        
        .ie-browser .block-grid.twelve-up .col,
        [owa] .block-grid.twelve-up .col {
            width: 50px !important;
        }
    </style>
    <style id="media-query" type="text/css">
        @media only screen and (min-width: 670px) {
            .block-grid {
                width: 650px !important;
            }
            .block-grid .col {
                vertical-align: top;
            }
            .block-grid .col.num12 {
                width: 650px !important;
            }
            .block-grid.mixed-two-up .col.num3 {
                width: 162px !important;
            }
            .block-grid.mixed-two-up .col.num4 {
                width: 216px !important;
            }
            .block-grid.mixed-two-up .col.num8 {
                width: 432px !important;
            }
            .block-grid.mixed-two-up .col.num9 {
                width: 486px !important;
            }
            .block-grid.two-up .col {
                width: 325px !important;
            }
            .block-grid.three-up .col {
                width: 216px !important;
            }
            .block-grid.four-up .col {
                width: 162px !important;
            }
            .block-grid.five-up .col {
                width: 130px !important;
            }
            .block-grid.six-up .col {
                width: 108px !important;
            }
            .block-grid.seven-up .col {
                width: 92px !important;
            }
            .block-grid.eight-up .col {
                width: 81px !important;
            }
            .block-grid.nine-up .col {
                width: 72px !important;
            }
            .block-grid.ten-up .col {
                width: 65px !important;
            }
            .block-grid.eleven-up .col {
                width: 59px !important;
            }
            .block-grid.twelve-up .col {
                width: 54px !important;
            }
        }
        
        @media (max-width: 670px) {
            .block-grid,
            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }
            .block-grid {
                width: 100% !important;
            }
            .col {
                width: 100% !important;
            }
            .col>div {
                margin: 0 auto;
            }
            img.fullwidth,
            img.fullwidthOnMobile {
                max-width: 100% !important;
            }
            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }
            .no-stack.two-up .col {
                width: 50% !important;
            }
            .no-stack .col.num4 {
                width: 33% !important;
            }
            .no-stack .col.num8 {
                width: 66% !important;
            }
            .no-stack .col.num4 {
                width: 33% !important;
            }
            .no-stack .col.num3 {
                width: 25% !important;
            }
            .no-stack .col.num6 {
                width: 50% !important;
            }
            .no-stack .col.num9 {
                width: 75% !important;
            }
            .video-block {
                max-width: none !important;
            }
            .mobile_hide {
                min-height: 0px;
                max-height: 0px;
                max-width: 0px;
                display: none;
                overflow: hidden;
                font-size: 0px;
            }
            .desktop_hide {
                display: block !important;
                max-height: none !important;
            }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #F5F5F5;">
    <style id="media-query-bodytag" type="text/css">
        @media (max-width: 670px) {
            .block-grid {
                min-width: 320px!important;
                max-width: 100%!important;
                width: 100%!important;
                display: block!important;
            }
            .col {
                min-width: 320px!important;
                max-width: 100%!important;
                width: 100%!important;
                display: block!important;
            }
            .col > div {
                margin: 0 auto;
            }
            img.fullwidth {
                max-width: 100%!important;
                height: auto!important;
            }
            img.fullwidthOnMobile {
                max-width: 100%!important;
                height: auto!important;
            }
            .no-stack .col {
                min-width: 0!important;
                display: table-cell!important;
            }
            .no-stack.two-up .col {
                width: 50%!important;
            }
            .no-stack.mixed-two-up .col.num4 {
                width: 33%!important;
            }
            .no-stack.mixed-two-up .col.num8 {
                width: 66%!important;
            }
            .no-stack.three-up .col.num4 {
                width: 33%!important
            }
            .no-stack.four-up .col.num3 {
                width: 25%!important
            }
        }
    </style>
    <!--[if IE]><div class="ie-browser"><![endif]-->
    <table bgcolor="#F5F5F5" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F5F5F5; width: 100%;" valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color:#F5F5F5"><![endif]-->
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="10" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 10px;" valign="top" width="100%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td height="10" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;" valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid two-up no-stack" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num6" style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right:25px; padding-left: 25px;">
                                            <!--<![endif]-->
                                            <div align="left" class="img-container left fixedwidth" style="padding-right: 0px;padding-left: 0px;"><img border="0" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 195px; display: block; margin: auto;" class="left fixedwidth" src="backEnd companyLogo" title="Image" width="195" alt="Company Logo"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #D6E7F0;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#D6E7F0;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:60px; padding-right: 25px; padding-left: 25px;">
                                            <div style="color:#052d3d;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:20px;padding-right:10px;padding-bottom:0px;padding-left:15px;">
                                                <div style="font-size: 12px; line-height: 18px; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; color: #052d3d;">
                                                    <p style="font-size: 14px; line-height: 75px; text-align: center; margin: 0;"><span style="font-size: 50px;"><strong><span style="line-height: 75px; font-size: 50px;"><span style="font-size: 38px; line-height: 57px;">WELCOME</span></span>
                                                        </strong>
                                                        </span>
                                                    </p>
                                                    <p style="font-size: 14px; line-height: 51px; text-align: center; margin: 0;"><span style="font-size: 34px;"><strong><span style="line-height: 51px; font-size: 34px;"><span style="color: #2190e3; line-height: 51px; font-size: 34px;">fname lname</span></span>
                                                        </strong>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div style="color:#555555;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 14px; color: #555555; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif;">
                                                    <p style="font-size: 14px; line-height: 21px; text-align: justify; margin: 0;"><span style="font-size: 18px; color: #000000;">I am firstName lastname and I`m a userJobTitle at companyName. I came across your profile as we`re currently looking for candidates with good skills and technology  and I think you could be a good fit.</span></p>
                                                </div>
                                            </div>
                                            
                                            <div align="center" class="button-container" style="padding-top:20px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <a href="baseUrl/vms/candidateregister" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #fc7318; border-radius: 15px; -webkit-border-radius: 15px; -moz-border-radius: 15px; width: auto; width: auto; border-top: 1px solid #fc7318; border-right: 1px solid #fc7318; border-bottom: 1px solid #fc7318; border-left: 1px solid #fc7318; padding-top: 10px; padding-bottom: 10px; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:40px;padding-right:40px;font-size:16px;display:inline-block;">
<span style="font-size: 16px; line-height: 32px;"><strong href="baseUrl/vms/candidateregister">CLICK HERE</strong></span>
</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:60px; padding-right: 0px; padding-left: 0px;">
                                            <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;" valign="top">
                                                            <table activate="activate" align="center" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: undefined; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" to="to" valign="top">
                                                                <tbody>
                                                                    <tr align="center" style="vertical-align: top; display: inline-block; text-align: center;" valign="top">
                                                                        <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 8px; padding-left: 8px; border-collapse: collapse;" valign="top">
                                                                            <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook" aria-hidden="true" style="width: 32px; height: 32px; background-color: #3b5998; border-radius: 50px; color: #fff; line-height: 32px;" title="Facebook" width="32" /></i></a>
                                                                        </td>
                                                                        <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 8px; padding-left: 8px; border-collapse: collapse;" valign="top">
                                                                            <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="width: 32px; height: 32px; background-color: #55acee; border-radius: 50px; color: #fff; line-height: 32px;" title="Facebook" width="32" /></i></a>
                                                                        </td>
                                                                        <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 8px; padding-left: 8px; border-collapse: collapse;" valign="top">
                                                                            <a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true" style="width: 32px; height: 32px; background-color: #cb1f27; border-radius: 50px; color: #fff; line-height: 32px;" title="Facebook" width="32" /></i></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="color:#555555;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 18px; color: #555555; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif;">
                                                    <p style="font-size: 14px; line-height: 21px; text-align: center; margin: 0;"><a href="website">companyName</a></p>
                                                </div>
                                            </div>
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; border-collapse: collapse;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 60%; border-top: 1px dotted #C4C4C4; height: 0px;" valign="top" width="60%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;" valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="color:#4F4F4F;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 14px; color: #4F4F4F; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 16px; text-align: center; margin: 0;"><span style="font-size: 14px;"><a href="#" rel="noopener" style="text-decoration: none; color: #2190E3;" target="_blank"><strong>Help&amp; FAQs</strong></a> | <a href="#" rel="noopener" style="text-decoration: none; color: #2190E3;" target="_blank"><strong>Returns</strong></a>  | <span style="background-color: transparent; line-height: 16px; font-size: 14px;">primaryContact</span></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>' WHERE `tedpros_email_template`.`templateFor` = 'addCandidateProfile';





UPDATE `tedpros_email_template` SET `template` = '<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <!--<![endif]-->
    <title></title>
    <!--[if !mso]><!-->
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table,
        td,
        tr {
            vertical-align: top;
            border-collapse: collapse;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        .ie-browser table {
            table-layout: fixed;
        }

        [owa] .img-container div,
        [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .block-grid,
        .ie-browser .num12,
        [owa] .num12,
        [owa] .block-grid {
            width: 600px !important;
        }

        .ie-browser .mixed-two-up .num4,
        [owa] .mixed-two-up .num4 {
            width: 200px !important;
        }

        .ie-browser .mixed-two-up .num8,
        [owa] .mixed-two-up .num8 {
            width: 400px !important;
        }

        .ie-browser .block-grid.two-up .col,
        [owa] .block-grid.two-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.three-up .col,
        [owa] .block-grid.three-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
            width: 150px !important;
        }

        .ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
            width: 120px !important;
        }

        .ie-browser .block-grid.six-up .col,
        [owa] .block-grid.six-up .col {
            width: 100px !important;
        }

        .ie-browser .block-grid.seven-up .col,
        [owa] .block-grid.seven-up .col {
            width: 85px !important;
        }

        .ie-browser .block-grid.eight-up .col,
        [owa] .block-grid.eight-up .col {
            width: 75px !important;
        }

        .ie-browser .block-grid.nine-up .col,
        [owa] .block-grid.nine-up .col {
            width: 66px !important;
        }

        .ie-browser .block-grid.ten-up .col,
        [owa] .block-grid.ten-up .col {
            width: 60px !important;
        }

        .ie-browser .block-grid.eleven-up .col,
        [owa] .block-grid.eleven-up .col {
            width: 54px !important;
        }

        .ie-browser .block-grid.twelve-up .col,
        [owa] .block-grid.twelve-up .col {
            width: 50px !important;
        }
    </style>
    <style id="media-query" type="text/css">
        @media only screen and (min-width: 620px) {
            .block-grid {
                width: 600px !important;
            }

            .block-grid .col {
                vertical-align: top;
            }

            .block-grid .col.num12 {
                width: 600px !important;
            }

            .block-grid.mixed-two-up .col.num3 {
                width: 150px !important;
            }

            .block-grid.mixed-two-up .col.num4 {
                width: 200px !important;
            }

            .block-grid.mixed-two-up .col.num8 {
                width: 400px !important;
            }

            .block-grid.mixed-two-up .col.num9 {
                width: 450px !important;
            }

            .block-grid.two-up .col {
                width: 300px !important;
            }

            .block-grid.three-up .col {
                width: 200px !important;
            }

            .block-grid.four-up .col {
                width: 150px !important;
            }

            .block-grid.five-up .col {
                width: 120px !important;
            }

            .block-grid.six-up .col {
                width: 100px !important;
            }

            .block-grid.seven-up .col {
                width: 85px !important;
            }

            .block-grid.eight-up .col {
                width: 75px !important;
            }

            .block-grid.nine-up .col {
                width: 66px !important;
            }

            .block-grid.ten-up .col {
                width: 60px !important;
            }

            .block-grid.eleven-up .col {
                width: 54px !important;
            }

            .block-grid.twelve-up .col {
                width: 50px !important;
            }
        }

        @media (max-width: 620px) {

            .block-grid,
            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .block-grid {
                width: 100% !important;
            }

            .col {
                width: 100% !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth,
            img.fullwidthOnMobile {
                max-width: 100% !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num8 {
                width: 66% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num3 {
                width: 25% !important;
            }

            .no-stack .col.num6 {
                width: 50% !important;
            }

            .no-stack .col.num9 {
                width: 75% !important;
            }

            .mobile_hide {
                min-height: 0px;
                max-height: 0px;
                max-width: 0px;
                display: none;
                overflow: hidden;
                font-size: 0px;
            }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #283C4B;">
    <style id="media-query-bodytag" type="text/css">
        @media (max-width: 620px) {
            .block-grid {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
                height: auto !important;
            }

            img.fullwidthOnMobile {
                max-width: 100% !important;
                height: auto !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack.mixed-two-up .col.num4 {
                width: 33% !important;
            }

            .no-stack.mixed-two-up .col.num8 {
                width: 66% !important;
            }

            .no-stack.three-up .col.num4 {
                width: 33% !important
            }

            .no-stack.four-up .col.num3 {
                width: 25% !important
            }
        }
    </style>
    <table bgcolor="#283C4B" cellpadding="0" cellspacing="0" class="nl-container"
        style="table-layout: fixed; vertical-align: top; min-width: 320px; margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #283C4B; width: 100%;"
        valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
                  
                    <div style="background-color:#283C4B;">

                    
                                         
                                  
                        <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #3AAEE0;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#3AAEE0;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#FFFFFF;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:120%;padding-top:30px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
                                                <div
                                                    style="font-size: 12px; line-height: 14px; color: #FFFFFF; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;text-align: -webkit-center;">
                                                    <p
                                                        style="font-size: 18px; line-height: 28px; text-align: center; margin: 10px;padding:10px;text-align: -webkit-center;background-color: #fff; padding: 11px;  border-radius: 7px; width:200px;">
                                                     
 <img align="center" alt="Image" border="0" class="center autowidth"
                                                        src="backEnd companyLogo"
                                                        style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; display: block;min-width:120px;max-width:180px;border-radius:5px;"
                                                        title="Image" width="100" />

                                                    </p>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:#283C4B;">
                        <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
                                                <div
                                                    style="font-size: 12px; line-height: 18px; color: #283C4B; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 24px; margin: 0;"><span
                                                            style="font-size: 16px;"><strong><span
                                                                    style="font-family: lucida sans unicode, lucida grande, sans-serif; line-height: 24px; font-size: 16px;">Hi REPORTINGNAME ,</span></strong> </span> </p>
                                                    <p style="font-size: 12px; line-height: 21px; margin: 0;"><span
                                                            style="font-family: lucida sans unicode, lucida grande, sans-serif; font-size: 14px;">                                                         USERNAME has submitted the time sheets.<br>Actions: Approve, Reject</span>
                                                    </p><br>
                                                    <div style="font-size: 12px; line-height: 24px; margin: 0;padding:10px 10px;background-color:#8080801a;border-top-right-radius: 35px;border-bottom-left-radius: 35px;">
    
                                <table style="width:90%;border-color:#968d8d57;" border=1 align="center">
                                    <tr>
                                        <th
                                            style=padding:5px;text-align: center;background-color: #3aaee0;color: white;width:20%;font-weight: bold;font-size: 16px;>
                                            Date(s)</th>
                                        <th
                                            style=padding:5px;text-align: center;background-color: #3aaee0;color: white;width:25%;font-weight: bold;font-size: 16px;>
                                            Project</th>
                                        <th
                                            style=padding:5px;text-align: center;background-color: #3aaee0;color: white;width:20%;font-weight: bold;font-size: 16px;>
                                            Activity</th>
                                        <th
                                            style=padding:5px;text-align: center;background-color: #3aaee0;color: white;width:20%;font-weight: bold;font-size: 16px;>
                                            Comment</th>
                                    </tr> 
                                    TIMESHEETDATA
                                </table>


                                                        </div>
                                                </div>
                                            </div>
                                           
                                            <div align="center" class="button-container"
                                                style="padding-top:25px;padding-right:0px;padding-bottom:0px;padding-left:0px;">
                                                <div
                                                    style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#3AAEE0;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;width:auto; width:auto;;border-top:1px solid #3AAEE0;border-right:1px solid #3AAEE0;border-bottom:1px solid #3AAEE0;border-left:1px solid #3AAEE0;padding-top:10px;padding-bottom:10px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;">
                                                    <span
                                                        style="padding-left:20px;padding-right:20px;font-size:14px;display:inline-block;"><span
                                                            style="font-size: 16px; line-height: 32px;"><span
                                                                data-mce-style="font-family: lucida sans unicode, lucida grande, sans-serif; font-size: 14px; line-height: 32px;"
                                                                mce-data-marked="1"
                                                                style="font-family: lucida sans unicode, lucida grande, sans-serif; font-size: 14px; line-height: 28px;text-decoration: none;"><a
                                                                    href="baseUrl/" style="color: #fff;text-decoration: none;"
                                                                    target="_blank" title="example">Update Status</a></span></span> </span> </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
                                                <div
                                                    style="font-size: 12px; line-height: 18px; color: #283C4B; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 24px; margin: 0;"><span
                                                            style="font-size: 16px;"><strong><span
                                                                    style="font-family: lucida sans unicode, lucida grande, sans-serif; line-height: 24px; font-size: 16px;">Thank You ,</span></strong> </span> </p>
                                                    <p style="font-size: 12px; line-height: 21px; margin: 0;"><span
                                                            style="font-family: lucida sans unicode, lucida grande, sans-serif; color:#9a9393;">
                                                            This is an automated email.</span>
                                                    </p><br>
                                                   
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>






                       
                    <div style="background-color:#283C4B;">
                        <div class="block-grid"
                            style="margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">


                                             <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:0px;padding-left:30px;">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>' WHERE `tedpros_email_template`.`templateFor` = 'timesheet_submit';

-- alter email template for addcandidateprofile, timesheet Sharmistha end
