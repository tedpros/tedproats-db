-- version : 20.1.V001.00  30th Jan,2020

-- user view changed by ravi start
DROP VIEW ` users`;

CREATE VIEW `users` AS select tedpros_user.user_id as id,tedpros_user.user_id as user_id ,concat(tedpros_user_personal_info.first_name ,' ',tedpros_user_personal_info.last_name ) as user_name,tedpros_user_personal_info.image as avatar,tedpros_user_login.token as token  from tedpros_user,tedpros_user_personal_info,tedpros_user_login where tedpros_user.user_id=tedpros_user_personal_info.user_id and tedpros_user.status='1' and tedpros_user.user_type='3' and tedpros_user.user_id=tedpros_user_login.user_id;
-- user view changes by ravi end
-- suresh start creating vendor employee field for vendor employee login

ALTER TABLE `tedpros_company_contact` ADD `vendor_employee` INT NOT NULL AFTER `company_id`;
-- suresh end 