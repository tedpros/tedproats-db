-- version : 20.1.V001.00  9th Mar,2020
-- create table for uploading company documents and role permission for uploading company documents added suresh --start

CREATE TABLE `tedpros_company_documents` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `company_id` VARCHAR(20) NOT NULL ,  `filename` TEXT NOT NULL ,  `document_type` VARCHAR(254) NOT NULL ,  `status` INT(11) NOT NULL ,  `title` VARCHAR(555) NOT NULL ,  `exp_date` VARCHAR(32) NOT NULL ,  `invoice_term` VARCHAR(254) NOT NULL ,  `added_by` VARCHAR(254) NOT NULL ,  `date_added` DATE NOT NULL ,  `date_updated` TIMESTAMP NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB


INSERT INTO `tedpros_role_activity` (`id`, `category_id`, `display_name`, `permission_name`, `permission`, `description`, `status`, `date_added`, `added_by`, `date_updated`) VALUES (NULL, '15', 'Upload CRM Documents', 'uploadCRMDocuments', 'create', 'Upload CRM Documents', '1', '03-09-2020', '1', current_timestamp());


-- create table for uploading company documents and role permission for uploading company documents added suresh -- end