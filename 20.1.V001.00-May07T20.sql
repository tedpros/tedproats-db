CREATE TABLE `tedpros_externalpost` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `userId` varchar(256) NOT NULL,
  `userName` text NOT NULL,
  `password` text NOT NULL,
  `status` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
);
INSERT INTO `tedpros_externalpost` (`id`, `name`, `userId`, `userName`, `password`, `status`, `date_created`, `date_updated`) VALUES
(1, 'DICE', 'RTX1d9813', 'WUkyVEcwcnl3UUFibUtQSmR0Y0Q3ZjNmWG1jT0dnL2k0S21FNTBaM3g4K25tSFBlQkZXbFlUZTF6VEhOcU1GUENVM3FFck5SbmVUejZ1Zm4wUTB0Y0E9PQ==', 'RlYvZEtSbzVKdHpXOW9JdHRpWW1GMEV6Q2o4VW9RamtHYTU5L0c0dno3Zm5jRHJsVlY1VGwvcmQvb05rRDhNRQ==', 1, '2020-04-28 20:41:08', '2020-05-06 12:22:42'),
(2, 'Tedpros', '', '', '', 1, '2020-05-05 19:14:20', '2020-05-05 19:14:20'),
(3, 'Facebook', '', '', '', 0, '2020-05-05 19:14:54', '2020-05-05 19:14:54'),
(4, 'LinkedIn', '', '', '', 0, '2020-05-05 19:14:54', '2020-05-05 19:14:54');
ALTER TABLE `tedpros_externalpost`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `tedpros_externalpost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;