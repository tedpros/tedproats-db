-- version : 20.1.V001.00  14th May,2020
-- alter scripts for tedpros candidate and office document tables uploading company documents added suresh --start

ALTER TABLE `tedpros_candidate` ADD `user_id` VARCHAR(32) NOT NULL AFTER `is_selected`;

ALTER TABLE `tedpros_office_document_type` CHANGE `type` `type` ENUM('1','2','3') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1';  

-- alter scripts for tedpros candidate and office document tables uploading company documents added suresh --end