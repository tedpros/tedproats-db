-- version : 20.1.V001.00  23rd Mar,2020
-- create table for tedpros_employee_delegation - sharmistha - start

CREATE TABLE `tedpros_employee_delegation` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `role` VARCHAR(254) NOT NULL ,  `activity` VARCHAR(254) NOT NULL ,  `delegation_empId` VARCHAR(128) NOT NULL ,  `emp_id` VARCHAR(128) NOT NULL ,  `delegation_status` VARCHAR(128) NOT NULL ,  `date_added` VARCHAR(32) NOT NULL ,  `added_by` VARCHAR(32) NOT NULL ,  `date_updated` TIMESTAMP NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

-- create table for tedpros_employee_delegation - sharmistha - end