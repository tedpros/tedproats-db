-- version : 20.1.V001.00  5th Mar,2020
-- insert user permissions - sharmistha - start

INSERT INTO `tedpros_category` (`id`, `category_name`, `description`, `date_added`, `added_by`, `date_updated`) VALUES (NULL, 'Manage Reports', 'View Sales, recruiters, leave, Timesheet and Audit reports', '2020-03-05', '1', CURRENT_TIMESTAMP);

INSERT INTO `tedpros_role_activity` (`id`, `category_id`, `display_name`, `permission_name`, `permission`, `description`, `status`, `date_added`, `added_by`, `date_updated`) VALUES (NULL, '30', 'View Reports', 'readReports', 'read', 'View Reports', '1', '2020-03-05', 'Administrator', CURRENT_TIMESTAMP);

-- insert user permissions - sharmistha - end